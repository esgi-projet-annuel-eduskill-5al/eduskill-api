FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /source

COPY ["EduSkill-API.csproj", "./"]
RUN dotnet restore "EduSkill-API.csproj"

COPY . .

WORKDIR "/source"

RUN dotnet build "EduSkill-API.csproj" -c Release -o /app/build

RUN dotnet publish "EduSkill-API.csproj" -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS final
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "EduSkill-API.dll"]
