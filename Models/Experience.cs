﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EduSkill_API.Models
{
    public class Experience
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Place { get; set; }
        public string? Location { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? Description { get; set; }

        public int PostulationId { get; set; }
        [ForeignKey("PostulationId")]
        public Postulation? Postulation { get; set; }
    }
}
