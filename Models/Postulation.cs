﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EduSkill_API.Models
{
    public class Postulation
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PostalCode { get; set; }
        public string? City { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }

        [ForeignKey("JobId")]
        public Job? Job { get; set; }

        public List<Experience>? Experiences { get; set; }
        public List<Availability>? Availability { get; set; }
        public List<Formation>? Formations { get; set; }
    }
}
