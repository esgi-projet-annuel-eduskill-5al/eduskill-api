﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EduSkill_API.Models
{
    public class Availability
    {
        public int Id { get; set; }
        public string? Date { get; set; }
        public string? StartTime { get; set; }
        public string? EndTime { get; set; }

        public int PostulationId { get; set; }
        [ForeignKey("PostulationId")]
        public Postulation? Postulation { get; set; }
    }
}
