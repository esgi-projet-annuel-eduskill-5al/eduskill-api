﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using EduSkill_API.Models;
using EduSkill_API.Datas;
using Microsoft.AspNetCore.Identity;

namespace EduSkill_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly JwtSettings _jwtSettings;
        private readonly EduskillDbContext _context;
        private readonly PasswordHasher<User> _passwordHasher;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IOptions<JwtSettings> jwtSettings, EduskillDbContext context, ILogger<AuthController> logger)
        {
            _jwtSettings = jwtSettings.Value;
            _context = context;
            _passwordHasher = new PasswordHasher<User>();
            _logger = logger;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Username == login.Username);

            if (user == null || !VerifyPassword(login.Password, user.Password))
            {
                return Unauthorized();
            }

            var token = GenerateJwtToken(user);
            return Ok(new { token });
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] LoginModel register)
        {
            var existingUser = await _context.Users.SingleOrDefaultAsync(u => u.Username == register.Username);
            if (existingUser != null)
            {
                return Conflict(new { message = "User already exists" });
            }

            var user = new User
            {
                Username = register.Username,
                Role = "Admin"
            };

            user.Password = _passwordHasher.HashPassword(user, register.Password);

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return Ok(new { message = "User registered successfully" });
        }

        private string GenerateJwtToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("Role", user.Role)
            };

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(_jwtSettings.DurationInMinutes),
                signingCredentials: credentials
            );

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.WriteToken(token);

            _logger.LogInformation("Generated JWT token: {jwtToken}", jwtToken);

            return jwtToken;
        }

        private bool VerifyPassword(string inputPassword, string storedPassword)
        {
            var verificationResult = _passwordHasher.VerifyHashedPassword(null, storedPassword, inputPassword);
            return verificationResult == PasswordVerificationResult.Success;
        }
    }
}
