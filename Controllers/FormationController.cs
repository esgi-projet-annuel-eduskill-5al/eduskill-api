﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EduSkill_API.Models;
using EduSkill_API.Datas;
using EduSkill_API.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace EduSkill_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormationController : ControllerBase
    {
        private readonly EduskillDbContext _context;

        public FormationController(EduskillDbContext context)
        {
            _context = context;
        }

        // GET: api/Formation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Formation>>> GetFormations()
        {
            return await _context.Formations.ToListAsync();
        }

        // GET: api/Formation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Formation>> GetFormation(int id)
        {
            var formation = await _context.Formations.FindAsync(id);

            if (formation == null)
            {
                return NotFound();
            }

            return formation;
        }

        // PUT: api/Formation/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFormation(int id, Formation formation)
        {
            if (id != formation.Id)
            {
                return BadRequest();
            }

            _context.Entry(formation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Formation
        [HttpPost]
        public async Task<ActionResult<Formation>> PostFormation(FormationDto formationDto)
        {
            var formation = new Formation
            {
                Title = formationDto.Title,
                Place = formationDto.Place,
                Location = formationDto.Location,
                StartDate = formationDto.StartDate,
                EndDate = formationDto.EndDate,
                Description = formationDto.Description,
                PostulationId = formationDto.PostulationId
            };

            _context.Formations.Add(formation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFormation", new { id = formation.Id }, formation);
        }

        // DELETE: api/Formation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFormation(int id)
        {
            var formation = await _context.Formations.FindAsync(id);
            if (formation == null)
            {
                return NotFound();
            }

            _context.Formations.Remove(formation);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FormationExists(int id)
        {
            return _context.Formations.Any(e => e.Id == id);
        }
    }
}
