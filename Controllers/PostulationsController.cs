﻿using EduSkill_API.Datas;
using EduSkill_API.Models;
using EduSkill_API.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EduSkill_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostulationsController : ControllerBase
    {
        private readonly EduskillDbContext _context;

        public PostulationsController(EduskillDbContext context)
        {
            _context = context;
        }

        // GET: api/Postulations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostulationDto>>> GetPostulations()
        {
            var postulations = await _context.Postulations
                .Include(p => p.Experiences)
                .Include(p => p.Availability)
                .Include(p => p.Formations)
                .ToListAsync();

            var postulationDtos = postulations.Select(p => new PostulationDto
            {
                Id = p.Id,
                JobId = p.JobId,
                FirstName = p.FirstName,
                LastName = p.LastName,
                PostalCode = p.PostalCode,
                City = p.City,
                Address = p.Address,
                Email = p.Email,
                Phone = p.Phone,
                Experiences = p.Experiences?.Select(e => new ExperienceDto
                {
                    Id = e.Id,
                    Title = e.Title,
                    Place = e.Place,
                    Location = e.Location,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Description = e.Description,
                    PostulationId = e.PostulationId
                }).ToList(),
                Availability = p.Availability?.Select(a => new AvailabilityDto
                {
                    Id = a.Id,
                    Date = a.Date,
                    StartTime = a.StartTime,
                    EndTime = a.EndTime,
                    PostulationId = a.PostulationId
                }).ToList(),
                Formations = p.Formations?.Select(f => new FormationDto
                {
                    Id = f.Id,
                    Title = f.Title,
                    Place = f.Place,
                    Location = f.Location,
                    StartDate = f.StartDate,
                    EndDate = f.EndDate,
                    Description = f.Description,
                    PostulationId = f.PostulationId
                }).ToList()
            });

            return Ok(postulationDtos);
        }

        // GET: api/Postulations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PostulationDto>> GetPostulation(int id)
        {
            var postulation = await _context.Postulations
                .Include(p => p.Experiences)
                .Include(p => p.Availability)
                .Include(p => p.Formations)
                .Include(p => p.Job)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (postulation == null)
            {
                return NotFound();
            }

            var postulationDto = new PostulationDto
            {
                Id = postulation.Id,
                JobId = postulation.JobId,
                FirstName = postulation.FirstName,
                LastName = postulation.LastName,
                PostalCode = postulation.PostalCode,
                City = postulation.City,
                Address = postulation.Address,
                Email = postulation.Email,
                Phone = postulation.Phone,
                Experiences = postulation.Experiences?.Select(e => new ExperienceDto
                {
                    Id = e.Id,
                    Title = e.Title,
                    Place = e.Place,
                    Location = e.Location,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Description = e.Description,
                    PostulationId = e.PostulationId
                }).ToList(),
                Availability = postulation.Availability?.Select(a => new AvailabilityDto
                {
                    Id = a.Id,
                    Date = a.Date,
                    StartTime = a.StartTime,
                    EndTime = a.EndTime,
                    PostulationId = a.PostulationId
                }).ToList(),
                Formations = postulation.Formations?.Select(f => new FormationDto
                {
                    Id = f.Id,
                    Title = f.Title,
                    Place = f.Place,
                    Location = f.Location,
                    StartDate = f.StartDate,
                    EndDate = f.EndDate,
                    Description = f.Description,
                    PostulationId = f.PostulationId
                }).ToList()
            };

            return Ok(postulationDto);
        }

        // PUT: api/Postulations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPostulation(int id, PostulationDto postulationDto)
        {
            if (id != postulationDto.Id)
            {
                return BadRequest();
            }

            var postulation = await _context.Postulations
                .Include(p => p.Experiences)
                .Include(p => p.Availability)
                .Include(p => p.Formations)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (postulation == null)
            {
                return NotFound();
            }

            postulation.JobId = postulationDto.JobId;
            postulation.FirstName = postulationDto.FirstName;
            postulation.LastName = postulationDto.LastName;
            postulation.PostalCode = postulationDto.PostalCode;
            postulation.City = postulationDto.City;
            postulation.Address = postulationDto.Address;
            postulation.Email = postulationDto.Email;
            postulation.Phone = postulationDto.Phone;

            _context.Entry(postulation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostulationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Postulations
        [HttpPost]
        public async Task<ActionResult<Postulation>> PostPostulation(PostulationDto postulationDto)
        {
            var postulation = new Postulation
            {
                JobId = postulationDto.JobId,
                FirstName = postulationDto.FirstName,
                LastName = postulationDto.LastName,
                PostalCode = postulationDto.PostalCode,
                City = postulationDto.City,
                Address = postulationDto.Address,
                Email = postulationDto.Email,
                Phone = postulationDto.Phone,
                Experiences = postulationDto.Experiences?.Select(e => new Experience
                {
                    Title = e.Title,
                    Place = e.Place,
                    Location = e.Location,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Description = e.Description,
                    PostulationId = e.PostulationId
                }).ToList(),
                Availability = postulationDto.Availability?.Select(a => new Availability
                {
                    Date = a.Date,
                    StartTime = a.StartTime,
                    EndTime = a.EndTime,
                    PostulationId = a.PostulationId
                }).ToList(),
                Formations = postulationDto.Formations?.Select(f => new Formation
                {
                    Title = f.Title,
                    Place = f.Place,
                    Location = f.Location,
                    StartDate = f.StartDate,
                    EndDate = f.EndDate,
                    Description = f.Description,
                    PostulationId = f.PostulationId
                }).ToList()
            };

            _context.Postulations.Add(postulation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPostulation", new { id = postulation.Id }, postulation);
        }

        // POST: api/Postulations/simple
        [HttpPost("simple")]
        public async Task<ActionResult<Postulation>> PostSimplePostulation(SimplePostulationDto simplePostulationDto)
        {
            var postulation = new Postulation
            {
                JobId = simplePostulationDto.JobId,
                FirstName = simplePostulationDto.FirstName,
                LastName = simplePostulationDto.LastName,
                PostalCode = simplePostulationDto.PostalCode,
                City = simplePostulationDto.City,
                Address = simplePostulationDto.Address,
                Email = simplePostulationDto.Email,
                Phone = simplePostulationDto.Phone
            };

            _context.Postulations.Add(postulation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPostulation", new { id = postulation.Id }, postulation);
        }

        // DELETE: api/Postulations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePostulation(int id)
        {
            var postulation = await _context.Postulations.FindAsync(id);
            if (postulation == null)
            {
                return NotFound();
            }

            _context.Postulations.Remove(postulation);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PostulationExists(int id)
        {
            return _context.Postulations.Any(e => e.Id == id);
        }
    }
}
