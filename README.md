
# Déploiement d'une application .NET sur Google Cloud Run

Ce guide décrit les étapes pour déployer une application .NET sur Google Cloud Run en utilisant Docker et Google Container Registry (GCR).

## Pré-requis

- Compte Google Cloud Platform (GCP) configuré
- Google Cloud SDK installé
- Docker installé
- Application .NET Core prête avec un `Dockerfile` configuré

## Étapes

### 1. Authentification à Google Cloud

Connectez-vous à votre compte Google Cloud et configurez votre projet :

```sh
gcloud auth login
gcloud config set project [VOTRE_PROJECT_ID ex: eduskill-427712]
```

### 2. Construction de l'image Docker

Naviguez vers le répertoire de votre application et construisez l'image Docker :

```sh
cd /path/to/your/application
docker build -t gcr.io/[VOTRE_PROJECT_ID ex: eduskill-427712]/[NOM_IMAGE ex: eduskill-api]:v1 .
```

### 3. Pousser l'image vers Google Container Registry

Configurez Docker pour utiliser l'authentification `gcloud` et poussez l'image Docker vers GCR :

```sh
gcloud auth configure-docker
docker push gcr.io/[VOTRE_PROJECT_ID ex: eduskill-427712]/[NOM_IMAGE ex: eduskill-api]:v1
```

### 4. Déploiement sur Google Cloud Run

Déployez l'image Docker sur Google Cloud Run :

```sh
gcloud run deploy [NOM_DU_SERVICE ex: eduskill-api] --image gcr.io/[VOTRE_PROJECT_ID ex: eduskill-427712]/[NOM_IMAGE ex: eduskill-api]:v1 --platform managed --region [REGION ex: us-central1] --allow-unauthenticated
```

### 5. Arrêt du service

Pour arrêter un service déployé sur Google Cloud Run, vous pouvez soit le supprimer, soit désactiver le trafic vers le service.

#### Supprimer le service

```sh
gcloud run services delete [NOM_DU_SERVICE ex: eduskill-api] --platform managed --region [REGION ex: us-central1]
```

#### Désactiver le trafic

```sh
gcloud run services update-traffic [NOM_DU_SERVICE ex: eduskill-api] --to-revisions=REVISION_NAME=0 --platform managed --region [REGION ex: us-central1]
```

## Conclusion

Ce guide fournit une méthode simple pour déployer une application .NET sur Google Cloud Run en utilisant Docker et GCR. Assurez-vous de vérifier toutes les étapes et de configurer correctement les permissions pour un déploiement réussi.
