﻿namespace EduSkill_API.Dtos
{
    public class PostulationDto
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PostalCode { get; set; }
        public string? City { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }

        public List<ExperienceDto>? Experiences { get; set; }
        public List<AvailabilityDto>? Availability { get; set; }
        public List<FormationDto>? Formations { get; set; }
    }
}
