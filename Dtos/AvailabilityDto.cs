﻿namespace EduSkill_API.Dtos
{
    public class AvailabilityDto
    {
        public int Id { get; set; }
        public string? Date { get; set; }
        public string? StartTime { get; set; }
        public string? EndTime { get; set; }
        public int PostulationId { get; set; }
    }
}
