﻿using Microsoft.EntityFrameworkCore;
using EduSkill_API.Models;

namespace EduSkill_API.Datas
{
    public class EduskillDbContext : DbContext
    {
        public EduskillDbContext(DbContextOptions<EduskillDbContext> options) : base(options)
        {
        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Availability> Availabilities { get; set; }
        public DbSet<Postulation> Postulations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Formation> Formations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Experience>()
                .HasOne(e => e.Postulation)
                .WithMany(p => p.Experiences)
                .HasForeignKey(e => e.PostulationId);

            modelBuilder.Entity<Availability>()
                .HasOne(a => a.Postulation)
                .WithMany(p => p.Availability)
                .HasForeignKey(a => a.PostulationId);

            modelBuilder.Entity<Formation>()
                .HasOne(f => f.Postulation)
                .WithMany(p => p.Formations)
                .HasForeignKey(f => f.PostulationId);
        }
    }
}
